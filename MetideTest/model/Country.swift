//
//  Country.swift
//  MetideTest
//
//  Created by Nizar Hamouda on 25/01/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import Foundation
import CoreLocation
import Darwin
class Country: Codable {
    var id: String?
    var enabled: String?
    var code3l: String?
    var code2l: String?
    var name: String?
    var name_official: String?
    var flag: String?
    var latitude: String?
    var longitude: String?
    var zoom: String?
    var distance : Double?
    var note: String?

    
    enum MemberKeys: String, CodingKey {
        case id
        case enabled
        case code3l
        case code2l
        case name
        case name_official
        case flag
        case latitude
        case longitude
        case zoom
    }
    
    init() {
        
    }
    
    init(  id: String,
     enabled: String,
     code3l: String,
     code2l: String,
     name: String,
     name_official: String,
     flag: String,
     latitude: String,
     longitude: String,
     zoom: String) {
        self.id = id
        self.enabled = enabled
        self.code3l = code3l
        self.code2l = code2l
        self.name = name
        self.name_official = name_official
        self.flag = flag
        self.latitude = latitude
        self.longitude = longitude
        self.zoom = zoom
        
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: MemberKeys.self)
        
   
        
        // Problematic property which can be String/Int
        if let str = try? container.decode(String.self, forKey: .id){
            id = str
        }
        else if let int = try? container.decode(Int.self, forKey: .id){
            id = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .enabled){
            enabled = str
        }
        else if let int = try? container.decode(Int.self, forKey: .enabled){
            enabled = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .code3l){
            code3l = str
        }
        else if let int = try? container.decode(Int.self, forKey: .code3l){
            code3l = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .code2l){
            code2l = str
        }
        else if let int = try? container.decode(Int.self, forKey: .code2l){
            code2l = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .name){
            name = str
        }
        
        if let str = try? container.decode(String.self, forKey: .name_official){
            name_official = str
        }
        
        if let str = try? container.decode(String.self, forKey: .flag){
            flag = str
        }
        
        if let str = try? container.decode(String.self, forKey: .latitude){
            latitude = str
        }
        else if let int = try? container.decode(Double.self, forKey: .latitude){
            latitude = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .longitude){
            longitude = str
        }
        else if let int = try? container.decode(Double.self, forKey: .longitude){
            longitude = String(int)
        }
        
        if let str = try? container.decode(String.self, forKey: .zoom){
            zoom = str
        }
        else if let int = try? container.decode(Int.self, forKey: .zoom){
            zoom = String(int)
        }
    }
    
}



