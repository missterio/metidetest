//
//  CountryFunctions.swift
//  MetideTest
//
//  Created by Nizar Hamouda on 27/01/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
class CountryFunctions {
    
    static let url =  URL(string: "https://us-central1-job-interview-cfe5a.cloudfunctions.net/countries")
    static let  METIDE_LATITUDE: Double = 45.5326976
    static let  METIDE_LONGITUDE: Double = 12.3932538
    
   static func download(appDelegate: AppDelegate, completition: @escaping () -> ()) {
    
    let context = appDelegate.persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Countries")
    request.returnsObjectsAsFaults = false
    

    
    guard let downloadURL = url else {return}
    let login = "developer"
    let password = "metide"
    let config = URLSessionConfiguration.default
    let userPasswordString = "\(login):\(password)"
    let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)
    let base64EncodedCredential = userPasswordData!.base64EncodedString()
    let authString = "Basic \(base64EncodedCredential)"
    config.httpAdditionalHeaders = ["Authorization" : authString]
    let session = URLSession(configuration: config)
    let task = session.dataTask(with: downloadURL) { (Data, URLResponse, Error) -> Void in
        guard let data = Data, Error == nil, URLResponse != nil else {
            print("something is wrong")
            do {
                if(Dataa.countriesArray.count > 0)
                {Dataa.countriesArray.removeAll()}
                let resulats = try context.fetch(request)
                
                
                if resulats.count > 0 {
                    for r in resulats as! [NSManagedObject] {
                        
                        
                        let  c = Country()
                        
                        let note = r.value(forKey: "note") as? String
                        if note != nil
                        {
                            c.note = note
                        }
                        else {
                            c.note = "type something..."
                        }
                        
                        if let name = r.value(forKey: "name") as? String {
                            c.name = name
                        }
                        
                        
                        if let name_official = r.value(forKey: "name_official") as? String {
                            c.name_official = name_official
                        }
                        
                        
                        if let id = r.value(forKey: "id") as? String {
                            c.id = id
                        }
                        
                        
                        if let latitude = r.value(forKey: "latitude") as? String {
                            c.latitude = latitude
                        } else {
                            c.latitude = "0.0"
                        }
                        
                        
                        if let longitude = r.value(forKey: "longitude") as? String {
                            c.longitude = longitude
                        } else {
                            c.longitude = "0.0"
                        }
                        
                        
            
                        
                        

                        Dataa.countriesArray.append(c)

          
                        
                    }
                    self.sortByDistance()
                    
                }
            } catch {
                
            }
            
            DispatchQueue.main.async {
                
                completition()
            }
            return }
        print("downloaded")
        
        do {
            let downloadedCountries = try JSONDecoder().decode([Country].self, from: data)
            Dataa.countriesArray = downloadedCountries
            self.sortByDistance()
            //deleteAllData(appDelegate: appDelegate)
            let resulats = try context.fetch(request)
            if resulats.count == 0
            {for c in Dataa.countriesArray {
                let newCountry = NSEntityDescription.insertNewObject(forEntityName: "Countries", into: context)
                newCountry.setValue(c.id!, forKey: "id")
                newCountry.setValue(c.name!, forKey: "name")
                newCountry.setValue(c.name_official!, forKey: "name_official")
                newCountry.setValue(c.latitude ?? "0", forKey: "latitude")
                newCountry.setValue(c.longitude ?? "0" , forKey: "longitude")
  

            }
            }
            
            do {
                try context.save()
                print("Context saved")
            } catch  {
                print("error")
            }
            
            DispatchQueue.main.async {
                
                completition()
            }
        }
        catch let error  {
            print(error)
        }
    }
    task.resume()
}
    
    static func sortByDistance(){
        let coordinate0 = CLLocation(latitude:METIDE_LATITUDE,longitude:  METIDE_LONGITUDE)
        for i in 0...Dataa.countriesArray.count-1{
            if Dataa.countriesArray[i].latitude != nil && Dataa.countriesArray[i].longitude != nil {
                let coordinate1 = CLLocation(latitude: Double(Dataa.countriesArray[i].latitude!)!, longitude: Double(Dataa.countriesArray[i].longitude!)!)
                let distanceInMeters = coordinate0.distance(from: coordinate1)
                Dataa.countriesArray[i].distance = distanceInMeters
                
            }
            else{
                Dataa.countriesArray[i].distance = 99999999.000000000
            }
        }
        Dataa.countriesArray = Dataa.countriesArray.sorted(by: {$0.distance! < $1.distance!})
    }
}
