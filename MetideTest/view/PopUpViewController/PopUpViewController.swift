//
//  PopUpViewController.swift
//  MetideTest
//
//  Created by Nizar Hamouda on 26/01/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import UIKit
import CoreData
class PopUpViewController: UIViewController {

    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var flagImg: UIImageView!
    @IBOutlet weak var longitudeLbl: UILabel!
    
    @IBOutlet weak var latitudeLbl: UILabel!

    @IBOutlet weak var popUpView: UIView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var descriptionTxt: UITextView!
    var country:Country?
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 10
        popUpView.layer.masksToBounds = true
        nameLbl.text = country?.name
        
        guard let latS = country?.latitude else { return }
        let latD = Double(latS)
        let latStr = String(format: "%.2f", latD!)
        
        guard let longS = country?.longitude else { return }
        let longD = Double(longS)
        let longStr = String(format: "%.2f", longD!)
        
        latitudeLbl.text = "Latitude: " + longStr
        longitudeLbl.text = "Longitude: " + latStr
        
        idLbl.text = "Id: " + (country?.id)!
        fullNameLbl.text = country?.name_official
        
        getNote()
        
        if(country?.flag != nil) {
        
        if let ThumbImageUrl = URL(string: country!.flag!) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: ThumbImageUrl)
                if let data = data {
                    
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.flagImg.image = image
                    }
                }
            }
        }
        }
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done,
                                         target: self, action: #selector(self.doneClicked))
        toolBar.setItems([doneButton], animated: true)
        descriptionTxt.inputAccessoryView = toolBar
    }
    
    @objc func doneClicked() {
        view.endEditing(true)
    }

    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveDescription(_ sender: Any) {
        updateNote()
        dismiss(animated: true, completion: nil)
    }
    
    func updateNote() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Countries")
        fetchRequest.predicate = NSPredicate(format: "id = %@", (country?.id)!)
        do {
            let test = try managedContext.fetch(fetchRequest)
            let object = test.first as! NSManagedObject
            object.setValue(descriptionTxt.text, forKey: "note")
            do {
                try managedContext.save()
            } catch {
                print(error)
            }
        } catch {
            print("error")
        }
        
    }
    
    func getNote() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Countries")
        fetchRequest.predicate = NSPredicate(format: "id = %@", (country?.id)!)
        do {
            let test = try managedContext.fetch(fetchRequest)
            let object = test.first as! NSManagedObject
            let note = object.value(forKey: "note")
            descriptionTxt.text = ((note ?? "type something...") as! String)
        } catch {
            print("error")
        }
        
    }
    
    

}
