//
//  CountryListViewController.swift
//  MetideTest
//
//  Created by Nizar Hamouda on 25/01/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
class CountryListViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!


    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var refreshControl: UIRefreshControl?
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = false
        CountryFunctions.download(appDelegate: appDelegate,completition: { [weak self] in
            // completion
            self?.activityIndicator.isHidden = true
            self?.tableview.reloadData()
        })
        addRefreshControl()
    }
    
    func addRefreshControl() {
        activityIndicator.isHidden = false
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.red
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableview.addSubview(refreshControl!)
    }
    
    @objc func refreshList() {
        
        
        //download()
        CountryFunctions.download(appDelegate: appDelegate,completition: { [weak self] in
            // completion
            self?.activityIndicator.isHidden = true
            self?.tableview.reloadData()
        })
        refreshControl?.endRefreshing()
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetail"){
            
            let countryDetails = segue.destination as! PopUpViewController
            
            if let indexPath = self.tableview.indexPathForSelectedRow {
                let selectedCountry = Dataa.countriesArray[indexPath.row]
                countryDetails.country = selectedCountry
            }
        }
    }
}


extension CountryListViewController: UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
        }
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

extension CountryListViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Dataa.countriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CountryTableViewCell
        
        
        cell.setup(country: Dataa.countriesArray[indexPath.row])
        
        return cell;
    }
    
    
}
