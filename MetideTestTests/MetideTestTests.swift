//
//  MetideTestTests.swift
//  MetideTestTests
//
//  Created by Nizar Hamouda on 25/01/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import XCTest
@testable import MetideTest

class MetideTestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDecodingStandardType() {
        let json = """
        {
        "id": "1",
        "enabled": "1",
        "code3l": "AFG",
        "code2l": "AF",
        "name": "Afghanistan",
        "name_official": "Islamic Republic of Afghanistan",
        "flag": "https://firebasestorage.googleapis.com/v0/b/job-interview-cfe5a.appspot.com/o/AF.png?alt=media",
        "latitude": "33.98299275",
        "longitude": "66.39159363",
        "zoom": "6"
        }
        """.data(using: .utf8)!
        
        let country = try! JSONDecoder().decode(Country.self, from: json)
        
        XCTAssertEqual(country.name, "Afghanistan")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
